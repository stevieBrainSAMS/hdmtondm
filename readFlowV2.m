function [ data ] = readFlowV2( filenameIn,varargin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
v=int64(2);
tagsFilename = sprintf('tagsV%d.mat',v)
load(tagsFilename)
assert(v==VTAG);

nVarargs = length(varargin);
if(nVarargs ~= 0)
    data = varargin{1};
end

try
    fileIdIn = fopen(filenameIn,'r','ieee-be');
    readTag(fileIdIn,HEADERSTART);
        readTag(fileIdIn,VERSION);
        data.version = fread(fileIdIn, 1, '*int64');
        assert(data.version == v);
        readTag(fileIdIn,TIMESTART);
            readTag(fileIdIn,ORIGINTIME);            
            data.originTime = fread(fileIdIn, 1, '*int64');
            readTag(fileIdIn,STARTTIME);            
            data.startTime = fread(fileIdIn, 1, '*int64');
            readTag(fileIdIn,DELTAT);
            data.deltat = fread(fileIdIn, 1, 'double');
            readTag(fileIdIn,NTIMESTEPS);
            data.nTimeSteps = fread(fileIdIn, 1, '*int32');
        readTag(fileIdIn,TIMEEND);
        readTag(fileIdIn,ELESTART);
            readTag(fileIdIn,NBNODES);
            data.nBNodes = fread(fileIdIn, 1, '*int32');
            readTag(fileIdIn,BNODEREFS);
            data.bNodeRefs = fread(fileIdIn, data.nBNodes, '*int32');
        readTag(fileIdIn,ELEEND);
        readTag(fileIdIn,LAYERSSTART);
            readTag(fileIdIn,NLAYERS);
            data.nLayers = fread(fileIdIn, 1, '*int32');
            readTag(fileIdIn,SIGMADEPTHS);
            data.sigmaDepths = fread(fileIdIn, data.nLayers, 'double');
        readTag(fileIdIn,LAYERSEND);
        readTag(fileIdIn,VALSSTART);
            readTag(fileIdIn,VALSPERBNODE);
            data.valsPerBNode = fread(fileIdIn, 1, '*int32');
            data.bNodeValNames = fread(fileIdIn, data.valsPerBNode, '*int64');
            readTag(fileIdIn,VALSPERFNODE);
            data.valsPerFNode = fread(fileIdIn, 1, '*int32');
            data.fNodeValNames = fread(fileIdIn, data.valsPerFNode, '*int64');
        readTag(fileIdIn,VALSEND);
    readTag(fileIdIn,HEADEREND);
    readTag(fileIdIn,DATASTART);
        data.bNodeVals = zeros(data.valsPerBNode,data.nBNodes,data.nTimeSteps);
        data.fNodeVals = zeros(data.valsPerFNode,data.nLayers,data.nBNodes,data.nTimeSteps);
        for timeIndex = 1:1:data.nTimeSteps
            readTag(fileIdIn,FRAMESTART);
            for nodeIndex = 1:1:data.nBNodes
                for valIndex = 1:1:data.valsPerBNode
                    data.bNodeVals(valIndex,nodeIndex,timeIndex)=fread(fileIdIn, 1, 'double');
                end
                for layerIndex = 1:1:data.nLayers
                    for valIndex = 1:1:data.valsPerFNode
                        data.fNodeVals(valIndex,layerIndex,nodeIndex,timeIndex)=fread(fileIdIn, 1, 'double');
                    end
                end
            end
            readTag(fileIdIn,FRAMEEND);
        end
    readTag(fileIdIn,DATAEND);
    fclose(fileIdIn);
catch exp
    fclose(fileIdIn);
    rethrow(exp);
end


end

