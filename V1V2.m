load('tagsV2.mat');
load('dictionaryV2.mat');
%change as required
filenameFlowIn = 'C:\Users\SA01TC\Documents\Carradale\carradaleV1.depomodflowmetrybinary';
filenameFlowOut = 'C:\Users\SA01TC\Documents\Carradale\carradaleV2.depomodflowmetrybinary';

%read in the previous version of the data
dataOut=readFlowV1(filenameFlowIn);

% change version
dataOut.version = int64(2);
% get these as epoch timesusing date2Epoch
% origin time is the start of the simulated period 
dataOut.originTime = date2Epoch('20170519 120000');
%starttime is the start of this chunk of cureent record
%in the case the chunk is 3 days after the origin
dataOut.startTime = date2Epoch('20170522 120000');

%add items from the directionary that are actually present in the data!!
dataOut.valsPerBNode = 1;
dataOut.bNodeValNames = DICTIONARY.BNODEVALS.ELEVATION;

%add items from the directionary that are actually present in the data!!
dataOut.valsPerFNode = 5;
dataOut.fNodeValNames(1) = DICTIONARY.FNODEVALS.UVELOCITY;
dataOut.fNodeValNames(2) = DICTIONARY.FNODEVALS.VVELOCITY;
dataOut.fNodeValNames(3) = DICTIONARY.FNODEVALS.WVELOCITY;
dataOut.fNodeValNames(4) = DICTIONARY.FNODEVALS.TEMPERATURE;
dataOut.fNodeValNames(5) = DICTIONARY.FNODEVALS.SALINITY;

%generate some elevation data
dataOut.bNodeVals = ones(1,dataOut.nBNodes,dataOut.nTimeSteps);
A = sin(2*pi*(0:1:23)./12.4167);
B = repmat(A,367,1);
dataOut.bNodeVals(1,:,:) = B;

%the fieled to store data has changed its name
flowData = dataOut.uv;
dataOut = rmfield(dataOut,'uv');
dataOut.fNodeVals = zeros(dataOut.valsPerFNode,dataOut.nLayers,dataOut.nBNodes,dataOut.nTimeSteps);
dataOut.fNodeVals(1:3,:,:,:) = flowData;
%10 degrees
dataOut.fNodeVals(4,:,:,:) = 10*ones(1,dataOut.nLayers,dataOut.nBNodes,dataOut.nTimeSteps);
%35 psu?
dataOut.fNodeVals(4,:,:,:) = 35*ones(1,dataOut.nLayers,dataOut.nBNodes,dataOut.nTimeSteps);

writeFlowV2(dataOut,filenameFlowOut);

disp(dataOut);

dataIn=readFlowV2(filenameFlowOut);

disp(dataIn);

