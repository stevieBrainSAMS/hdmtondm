function [ sizeInBytes ] = writeMikeMesh(meshStruct)
    if ~isfield(meshStruct,'fileName')
        error( 'meshStruct does not contain the filename field');
    else
        fileName=meshStruct.fileName;
    end

mikeMagicNumber=100079;
mikeMeterUnits=1000;
mikeNodesPerElement=3;
mikeTrianglesOtherEntry=21;


%nodes
dataPrecision=3; %default
precisionString=sprintf('%%.%df',dataPrecision);
nodeHeader = sprintf('%d %d %d %s',mikeMagicNumber,mikeMeterUnits,length(meshStruct.nodeRefs),meshStruct.crs);
fid = fopen(fileName,'w');
fprintf(fid,'%s\r\n',nodeHeader);
fclose(fid);
dlmwrite(fileName, meshStruct.nodeRefs,'-append','delimiter',' ','precision',precisionString);
%trinagles
dataPrecision=0; %default
precisionString=sprintf('%%.%df',dataPrecision);
triangleHeader = sprintf('%d %d %d',length(meshStruct.triangleRefs),mikeNodesPerElement,mikeTrianglesOtherEntry);
fid = fopen(fileName,'a');
fprintf(fid,'%s\r\n',triangleHeader);
fclose(fid);
dlmwrite(fileName, meshStruct.triangleRefs,'-append','delimiter',' ','precision',precisionString);

sizeInBytes = dir(fileName);
sizeInBytes = sizeInBytes.bytes;

end

