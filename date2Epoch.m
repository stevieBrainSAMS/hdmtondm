function [ epochNow ] = date2Epoch( dateTime )
    %result of datenum('19700101 000000','yyyymmdd HHMMSS')
    epoch=719529;
    now=datenum(dateTime,'yyyymmdd HHMMSS');
    epochNow=int64((now-epoch)*86400000);
end

