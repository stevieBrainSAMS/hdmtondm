function [ count ] = writeFlowV1( data,filenameOut )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
v=int64(1);
tagsFilename = sprintf('tagsV%d.mat',v);
load(tagsFilename);
assert(v==VTAG);

try
    count = 0;
    fileIdOut = fopen(filenameOut,'w','ieee-be');    
    count=writeTag(fileIdOut,HEADERSTART,count);
        count=writeTag(fileIdOut,VERSION,count);
        count=count+fwrite(fileIdOut, v, 'int64');
        count=writeTag(fileIdOut,TIMESTART,count);
            count=writeTag(fileIdOut,DELTAT,count);
            count=count+fwrite(fileIdOut, data.deltat, 'double');
            count=writeTag(fileIdOut,NTIMESTEPS,count);
            count=count+fwrite(fileIdOut, data.nTimeSteps, 'int32');
            count=writeTag(fileIdOut,ORIGINTIME,count);            
        count=writeTag(fileIdOut,TIMEEND,count);
        count=writeTag(fileIdOut,ELESTART,count);
            count=writeTag(fileIdOut,NBNODES,count);
            count=count+fwrite(fileIdOut, data.nBNodes, 'int32');
            count=writeTag(fileIdOut,BNODEREFS,count);
            for refIndex = 1:1:data.nBNodes
                count=count+fwrite(fileIdOut, data.bNodeRefs(refIndex), 'int32');
            end
        count=writeTag(fileIdOut,ELEEND,count);
        count=writeTag(fileIdOut,LAYERSSTART,count);
            count=writeTag(fileIdOut,NLAYERS,count);
            count=count+fwrite(fileIdOut, data.nLayers, 'int32');
            count=writeTag(fileIdOut,SIGMADEPTHS,count);
            assert(data.nLayers==int32(max(size(data.sigmaDepths))));
            for sigIndex = 1:1:data.nLayers
                count=count+fwrite(fileIdOut, data.sigmaDepths(sigIndex), 'double');
            end
        count=writeTag(fileIdOut,LAYERSEND,count);
        count=writeTag(fileIdOut,VALSSTART,count);
            count=writeTag(fileIdOut,VALSPERFNODE,count);
            count=count+fwrite(fileIdOut, data.valsPerFNode, 'int32');         
        count=writeTag(fileIdOut,VALSEND,count);
    count=writeTag(fileIdOut,HEADEREND,count);
    count=writeTag(fileIdOut,DATASTART,count);
    uvDims= size(data.uv);
    assert(uvDims(1)==data.valsPerFNode);
    assert(uvDims(2)==data.nLayers);
    assert(uvDims(3)==data.nBNodes);
    assert(uvDims(4)==data.nTimeSteps);
    for timeIndex = 1:1:data.nTimeSteps
        count=writeTag(fileIdOut,FRAMESTART,count);
        for nodeIndex = 1:1:data.nBNodes
            count=count+fwrite(fileIdOut, data.elevation(nodeIndex,timeIndex), 'double');
            for layerIndex = 1:1:data.nLayers
                for valIndex = 1:1:data.valsPerFNode
                    count=count+fwrite(fileIdOut, data.uv(valIndex,layerIndex,nodeIndex,timeIndex), 'double');
                end
            end
        end
        count=writeTag(fileIdOut,FRAMEEND,count);
    end
    count=writeTag(fileIdOut,DATAEND,count);
    fclose(fileIdOut);
catch exp;
    fclose(fileIdOut);
    rethrow(exp)
end    

end