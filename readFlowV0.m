function [ data ] = readFlowV0( filenameIn,varargin )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
v=int64(0);
tagsFilename = sprintf('tagsV%d.mat',v);
load(tagsFilename);
assert(v==VTAG);

nVarargs = length(varargin);
if(nVarargs ~= 0)
    data = varargin{1};
end
try
    fileIdIn = fopen(filenameIn,'r','ieee-be');
    readTag(fileIdIn,HEADERSTART);
        readTag(fileIdIn,VERSION);
        data.version = fread(fileIdIn, 1, '*int64');
        assert(data.version == v);
        readTag(fileIdIn,TIMESTART);
            readTag(fileIdIn,DELTAT);
            data.deltat = fread(fileIdIn, 1, 'int64');
            readTag(fileIdIn,NTIMESTEPS);
            data.nTimeSteps = fread(fileIdIn, 1, '*int64');
        readTag(fileIdIn,TIMEEND);
        readTag(fileIdIn,ELESTART);
            readTag(fileIdIn,NNODES);
            data.nNodes = fread(fileIdIn, 1, '*int64');
        readTag(fileIdIn,ELEEND);
        readTag(fileIdIn,LAYERSSTART);
            readTag(fileIdIn,NLAYERS);
            data.nLayers = fread(fileIdIn, 1, '*int64');
            readTag(fileIdIn,SIGMADEPTHS);
            data.sigmaDepths = fread(fileIdIn, data.nLayers+1, 'double');
        readTag(fileIdIn,LAYERSEND);
    readTag(fileIdIn,HEADEREND);
        data.valsPerFNode = 3;
        data.uv = zeros(data.valsPerFNode,data.nLayers,data.nNodes,data.nTimeSteps);
    readTag(fileIdIn,DATASTART);
    for timeIndex = 1:1:data.nTimeSteps
        readTag(fileIdIn,FRAMESTART);
        for nodeIndex = 1:1:data.nNodes
            for layerIndex = 1:1:data.nLayers
                for valIndex = 1:1:data.valsPerFNode
                    data.uv(valIndex,layerIndex,nodeIndex,timeIndex)=fread(fileIdIn, 1, 'double');
                end
            end
        end
        readTag(fileIdIn,FRAMEEND);
    end
    readTag(fileIdIn,DATAEND);
    fclose(fileIdIn);
catch exp
    fclose(fileIdIn);
    rethrow(exp);
end


end

