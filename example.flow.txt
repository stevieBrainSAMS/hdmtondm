Always bigendian endcoding.
values in brackets are base 36 rep of preceeding value base 10 rep.
ignore all entries like [9223372036854775807L] they are for my usage and should not appear in the file.
binary files do not include line feeds, tabs or spaces. They appear below to make it easier for humans to read.
Key
deltaT = period of time samples in seconds
nTimeSteps = number of time samples
originTime = Epoch at the origin of the entire period of model output
startTime = Epoch at the start of the period of model output in THIS file
nBNodes = number of bathyemetry element vertices
N(n) = reference number of nth node in same order as velocities
nLayers = number of layers (sampling pionts in vertical direction)
Zs(n) = sigma depth of layer n [0,-1] velocities below should be in same layer order
valsPerFNode = number of sampled quanities per node (vals below shown for the case valsPerFNode = 3) 
U(l,n,t) = U velocity sample of layer l for node n at time t
V(l,n,t) = V velocity sample of layer l for node n at time t
W(l,n,t) = V velocity sample of layer l for node n at time t
[Double] = double precison ieee fromat floating point number 
[Long] = 64 bit signed integer 
[Int] = 32 bit signed integer 

------------------File Starts Below-----------------------------------
  [Long] 63605794863571913L (HEADERSTART) 														00 e1 f9 22 28 7f e3 c9
    [9223372036854775807L]
	[Long] 68373459095L (VERSION)   															00 00 00 0f eb 60 30 97
      [9223372036854775807L]
		[Long] data.v
	[Long] 83271526404041L (TIMESTART)															00 00 4b bc 29 c0 4f c9
	 [9223372036854775807L]
		[Long] 2515055439413894L (ORIGINTIME)
		   [9223372036854775807L]
			[Long] data.originTime
		[Long] 81287088663686L (STARTTIME)
		 [9223372036854775807L]
			[Long] data.startTime
		[Long] 810592661L (DELTAT)																00 00 00 00 30 50 a9 95
	[9223372036854775807L]
			[Double] data.deltaT
		[Long] 2419150529782720L (NTIMESTEPS)													00 08 98 34 5f fc 63 c0
		   [9223372036854775807L]
			[Int] data.nTimeSteps
	[Long] 64252702633L (TIMEEND)																00 00 00 0e f5 c2 6f a9
  [9223372036854775807L]
	[Long] 1143705649097L (ELESTART)															00 00 01 0a 4a 2b 3f c9
    [9223372036854775807L]
		[Long] 50770889956L (NBNODES)															00 00 00 0B D2 2E 4C E4
	  [9223372036854775807L]
			[Int] data.nBNodes
		[Long] 32887638385624L (BNODEREFS)														00 00 1D E9 40 00 E3 D8
		 [9223372036854775807L]
			[Int] data.bNodeRefs(1)
			[Int] data.bNodeRefs(2)
			...
			[Int] data.bNodeRefs(data.nBNodes)
	[Long] 882470569L (ELEEND)																	00 00 00 00 34 99 6e a9
[9223372036854775807L]
	[Long] 77892002157729737L (LAYERSSTART)														01 14 ba 5c ff 74 97 c9
	    [9223372036854775807L]
		[Long] 51354185032L (NLAYERS)															00 00 00 0b f4 f2 ad 48
	  [9223372036854775807L]
			[Int] data.nLayers
		[Long] 104247399890527120L (SIGMADEPTHS)												01 72 5c 75 37 3b a3 90
		     [9223372036854775807L]
			[Double] data.sigmaDepths(1)
			[Double] data.sigmaDepths(2)
			...
			[Double] data.sigmaDepths(data.nLayers)
	[Long] 60101853498409L (LAYERSEND)															00 00 36 a9 8d a0 cc 29
	 [9223372036854775807L]
	[Long] 88285502650313L (VALSSTART)															00 00 50 4B 92 06 37 C9
	 [9223372036854775807L]
		[Long] 4119048144909578786L (VALSPERBNODE)
		      [9223372036854775807L]
			[Int] data.valsPerBNode
		[Long] 32887638567280L (BNODEVALS)
		 [9223372036854775807L]
			[Long] data.bNodeValNames(1)	//expected to be 41173553727959 (ELEVATION)
			{Long} data.bNodeValNames(2)
			...
			[Long] data.bNodeValNames(data.valsPerBNode)
		[Long] 4119048144916297250L (VALSPERFNODE)												39 29 CC 76 D2 77 B6 22
		      [9223372036854775807L]
			[Int] valsPerFNode
		[Long] 44172078197104L (FNODEVALS)
		     [9223372036854775807L]
			[Long] data.fNodeValNames(1)	//expected to be 87094371948118 (UVELOCITY)
			[Long] data.fNodeValNames(2)	//expected to be 89915481855574 (VVELOCITY)
			[Long] data.fNodeValNames(3)	//expected to be 92736591763030 (WVELOCITY)
			{Long} data.fNodeValNames(4)	//optional
			...
			{Long} data.fNodeValNames(data.valsPerFNode)	//optional
			[Long] 68121511465L (VALSEND)														00 00 00 0F DC 5B C6 29
      [9223372036854775807L]
  [Long] 49078545401065L (HEADEREND)															00 00 2c a2 fd 43 c4 e9
   [9223372036854775807L]
  [Long] 37521850183625L (DATASTART)															00 00 22 20 3c 8d 2f c9
   [9223372036854775807L]
	[Long] 1600401743302601L (FRAMESTART)														00 05 af 8e 91 68 4f c9
	   [9223372036854775807L]
		[Double] U(1,1,1)
		[Double] V(1,1,1)
		[Double] W(1,1,1)
		{Double} ...
		...
		[Double] U(nLayers,1,1)
		[Double] V(nLayers,1,1)
		[Double] W(nLayers,1,1)
		[Double] U(1,2,1)
		[Double] V(1,2,1)
		[Double] W(1,2,1)
		...
		[Double] U(nLayers,2,1)
		[Double] V(nLayers,2,1)
		[Double] W(nLayers,2,1)
		... ... ...
		[Double] U(nLayers,nNodes,1)
		[Double] V(nLayers,nNodes,1)
		[Double] W(nLayers,nNodes,1)
	[Long] 1234877869993L (FRAMEEND)											00 00 01 1f 84 74 ef a9
	[9223372036854775807L]
	[Long] 1600401743302601L (FRAMESTART)										00 05 af 8e 91 68 4f c9
	   [9223372036854775807L]
		[Double] U(1,1,2)
		[Double] V(1,1,2)
		[Double] W(1,1,2)
		...
		[Double] U(nLayers,1,2)
		[Double] V(nLayers,1,2)
		[Double] W(nLayers,1,2)
		[Double] U(1,2,2)
		[Double] V(1,2,2)
		[Double] W(1,2,2)
		...
		[Double] U(nLayers,2,2)
		[Double] V(nLayers,2,2)
		[Double] W(nLayers,2,2)
		... ... ...
		[Double] U(nLayers,nNodes,2)
		[Double] V(nLayers,nNodes,2)
		[Double] W(nLayers,nNodes,2)
	[Long] 1234877869993L (FRAMEEND)											00 00 01 1f 84 74 ef a9
	[9223372036854775807L]
	[Long] 1600401743302601L (FRAMESTART)										00 05 af 8e 91 68 4f c9
	   [9223372036854775807L]
	... ... ... ... ... ...
	[Long] 1234877869993L (FRAMEEND)											00 00 01 1f 84 74 ef a9
	[9223372036854775807L]
	[Long] 1600401743302601L (FRAMESTART)										00 05 af 8e 91 68 4f c9
	   [9223372036854775807L]
		[Double] U(1,1,nTimeSteps)
		[Double] V(1,1,nTimeSteps)
		[Double] W(1,1,nTimeSteps)
		...
		[Double] U(nLayers,1,nTimeSteps)
		[Double] V(nLayers,1,nTimeSteps)
		[Double] W(nLayers,1,nTimeSteps)
		[Double] U(1,2,nTimeSteps)
		[Double] V(1,2,nTimeSteps)
		[Double] W(1,2,nTimeSteps)
		...
		[Double] U(nLayers,2,nTimeSteps)
		[Double] V(nLayers,2,nTimeSteps)
		[Double] W(nLayers,2,nTimeSteps)
		... ... ...
		[Double] U(nLayers,nNodes,nTimeSteps)
		[Double] V(nLayers,nNodes,nTimeSteps)
		[Double] W(nLayers,nNodes,nTimeSteps)
	[Long] 1234877869993L (FRAMEEND)											00 00 01 1f 84 74 ef a9
	[9223372036854775807L]
  [Long] 28952026537L (DATAEND)													00 00 00 06 bd ac dd a9
[9223372036854775807L]
------------------File Ends Above-----------------------------------
