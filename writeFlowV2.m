function [ count ] = writeFlowV2( data,filenameOut )
    %UNTITLED3 Summary of this function goes here
    %   Detailed explanation goes here
    v=int64(2);
    tagsFilename = sprintf('tagsV%d.mat',v);
    load(tagsFilename);
    assert(v==VTAG);

    try
        count = 0;
        fileIdOut = fopen(filenameOut,'w','ieee-be');    
        count=writeTag(fileIdOut,HEADERSTART,count);
            count=writeTag(fileIdOut,VERSION,count);
            count=count+fwrite(fileIdOut, v, 'int64');
            count=writeTag(fileIdOut,TIMESTART,count);
                count=writeTag(fileIdOut,ORIGINTIME,count);            
                count=count+fwrite(fileIdOut, data.originTime, 'int64');
                count=writeTag(fileIdOut,STARTTIME,count);            
                count=count+fwrite(fileIdOut, data.startTime, 'int64');
                count=writeTag(fileIdOut,DELTAT,count);
                count=count+fwrite(fileIdOut, data.deltat, 'double');
                count=writeTag(fileIdOut,NTIMESTEPS,count);
                count=count+fwrite(fileIdOut, data.nTimeSteps, 'int32');
            count=writeTag(fileIdOut,TIMEEND,count);
            count=writeTag(fileIdOut,ELESTART,count);
                count=writeTag(fileIdOut,NBNODES,count);
                count=count+fwrite(fileIdOut, data.nBNodes, 'int32');
                count=writeTag(fileIdOut,BNODEREFS,count);
                for refIndex = 1:1:data.nBNodes
                    count=count+fwrite(fileIdOut, data.bNodeRefs(refIndex), 'int32');
                end
            count=writeTag(fileIdOut,ELEEND,count);
            count=writeTag(fileIdOut,LAYERSSTART,count);
                count=writeTag(fileIdOut,NLAYERS,count);
                count=count+fwrite(fileIdOut, data.nLayers, 'int32');
                count=writeTag(fileIdOut,SIGMADEPTHS,count);
                assert(data.nLayers==int32(max(size(data.sigmaDepths))));
                for sigIndex = 1:1:data.nLayers
                    count=count+fwrite(fileIdOut, data.sigmaDepths(sigIndex), 'double');
                end
            count=writeTag(fileIdOut,LAYERSEND,count);
            count=writeTag(fileIdOut,VALSSTART,count);
                count=writeTag(fileIdOut,VALSPERBNODE,count);
                count=count+fwrite(fileIdOut, data.valsPerBNode, 'int32');
                for valsIndex = 1:1:data.valsPerBNode
                    count=count+fwrite(fileIdOut, data.bNodeValNames(valsIndex), 'int64');
                end            
                count=writeTag(fileIdOut,VALSPERFNODE,count);
                count=count+fwrite(fileIdOut, data.valsPerFNode, 'int32');         
                for valsIndex = 1:1:data.valsPerFNode
                    count=count+fwrite(fileIdOut, data.fNodeValNames(valsIndex), 'int64');
                end            
            count=writeTag(fileIdOut,VALSEND,count);
        count=writeTag(fileIdOut,HEADEREND,count);
        count=writeTag(fileIdOut,DATASTART,count);

        bNodeValsDims= size(data.bNodeVals);
        assert(bNodeValsDims(1)==data.valsPerBNode);
        assert(bNodeValsDims(2)==data.nBNodes);
        assert(bNodeValsDims(3)==data.nTimeSteps);    
        fNodeValsDims= size(data.fNodeVals);
        assert(fNodeValsDims(1)==data.valsPerFNode);
        assert(fNodeValsDims(2)==data.nLayers);
        assert(fNodeValsDims(3)==data.nBNodes);
        assert(fNodeValsDims(4)==data.nTimeSteps);
        for timeIndex = 1:1:data.nTimeSteps
            count=writeTag(fileIdOut,FRAMESTART,count);
            for nodeIndex = 1:1:data.nBNodes
                for valIndex = 1:1:data.valsPerBNode
                    count=count+fwrite(fileIdOut, data.bNodeVals(valIndex,nodeIndex,timeIndex), 'double');
                end
                for layerIndex = 1:1:data.nLayers
                    for valIndex = 1:1:data.valsPerFNode
                        count=count+fwrite(fileIdOut, data.fNodeVals(valIndex,layerIndex,nodeIndex,timeIndex), 'double');
                    end
                end
            end
            count=writeTag(fileIdOut,FRAMEEND,count);
        end
        count=writeTag(fileIdOut,DATAEND,count);
        fclose(fileIdOut);
    catch exp;
        fclose(fileIdOut);
        rethrow(exp)
    end    

end